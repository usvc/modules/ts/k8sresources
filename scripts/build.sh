#!/bin/sh
printf -- "\033[1m\\n\\n> building main library...\\n\\n\033[0m" \
  && tsc --project ./src \
  && printf -- "\033[1m\\n\\n> copying metadata to ./lib...\\n\\n\033[0m" \
  && (cp ./* ./lib/ || cp ./.* ./lib || printf -- '') \
  && sed -i -e 's|\.ts"|\.js"|g' ./lib/package.json \
  && sed -i -e 's|"__|"|g' ./lib/package.json \
  && sed -i '/"dev"\:/d' ./lib/package.json \
  && sed -i '/"build"\:/d' ./lib/package.json \
  && sed -i '/"prebuild"\:/d' ./lib/package.json \
  && printf -- "\033[1m\\n\\n> done! now navigate to ./lib - 'cd ./lib' - and run 'npm publish' from there!\\n\\n\033[0m";
