import { Deployment as v1Deployment } from 'k8stypes/apps/v1/Deployment';
import { Deployment as v1beta1Deployment } from 'k8stypes/apps/v1beta1/Deployment';
import { Deployment as v1beta2Deployment } from 'k8stypes/apps/v1beta2/Deployment';

import { NamespacedResourceArgs } from './common';

export const v1 = getNewDeploymentGenerator('apps/v1');
export const v1beta1 = getNewDeploymentGenerator('apps/v1beta1');
export const v1beta2 = getNewDeploymentGenerator('apps/v1beta2');

export type Deployment = v1Deployment | v1beta1Deployment | v1beta2Deployment;
export interface DeploymentGeneratorArgs extends NamespacedResourceArgs {
  annotations ?: {[key: string] : string}
};
export type NewDeploymentGenerator = (args : DeploymentGeneratorArgs) => Deployment;
function getNewDeploymentGenerator(apiVersion : string) : NewDeploymentGenerator {
  return function({
    name,
    namespace = 'default',
    annotations = {},
  } : DeploymentGeneratorArgs) : Deployment {
    const labels = {
      appName: name,
      generatedBy: 'k8sresources',
      generatedOn: (new Date()).toISOString(),
    };
    return {
      apiVersion,
      kind: 'Deployment',
      metadata: {
        name,
        namespace,
        annotations,
        labels,
      },
      spec: {
        selector: {
          matchLabels: labels,
        },
        replicas: 1,
        template: {
          metadata: {
            name,
            labels,
          },
          spec: {
            containers: [],
            initContainers: [],
          },
        },
      },
    };
  }
};
