import { Service as v1Service } from 'k8stypes/core/v1/Service';
import { ServicePort } from 'k8stypes/core/v1/ServicePort';

import { NamespacedResourceArgs } from './common';

export const v1 = getNewServiceGenerator('apps/v1');

export type Deployment = v1Service;
export interface ServiceGeneratorArgs extends NamespacedResourceArgs {
  annotations ?: {[key: string]: string},
  selectorLabels ?: {[key: string]: string},
  ports ?: ServicePort[],
};
export type NewServiceGenerator = (args : ServiceGeneratorArgs) => Deployment;
function getNewServiceGenerator(apiVersion : string) : NewServiceGenerator {
  return function({
    name,
    namespace = 'default',
    annotations = {},
    selectorLabels = {},
    ports = [],
  } : ServiceGeneratorArgs) : Deployment {
    const labels = {
      appName: name,
      generatedBy: 'k8sresources',
      generatedOn: (new Date()).toISOString(),
    };
    return {
      apiVersion,
      kind: 'Service',
      metadata: {
        name,
        namespace,
        annotations,
        labels,
      },
      spec: {
        selector: selectorLabels,
        ports: ports,
      },
    };
  }
};
