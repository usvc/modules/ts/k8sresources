import { v1 as appsv1, v1beta1 as appsv1beta1, v1beta2 as appsv1beta2 } from './apps';
import { v1 as corev1 } from './core';
import { DeploymentGeneratorArgs, Deployment } from './deployments';
import { ServiceGeneratorArgs } from './services';

export const apps = {
  v1: appsv1,
  v1beta1: appsv1beta1,
  v1beta2: appsv1beta2,
};

export const deployment = {
  'apps/v1': apps.v1.deployment,
  'apps/v1beta1': apps.v1beta1.deployment,
  'apps/v1beta2': apps.v1beta2.deployment,
};
export type DeploymentVersions = keyof typeof deployment;
export function createDeployment(version : DeploymentVersions, args : DeploymentGeneratorArgs) : Deployment {
  return deployment[version](args);
};

export const core = {
  v1: corev1,
};

export const service = {
  'core/v1': core.v1.service,
};

export function createService(args : ServiceGeneratorArgs) {
  return service['core/v1'](args);
};
