export interface NamespacedResourceArgs extends NamedResourceArgs {
  namespace ?: string,
};

export interface NamedResourceArgs {
  name : string,
};
