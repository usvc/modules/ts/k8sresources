import { v1 as v1Deployment } from './deployments';
import { v1beta1 as v1beta1Deployment } from './deployments';
import { v1beta2 as v1beta2Deployment } from './deployments';

export const v1 = {
  deployment: v1Deployment,
};

export const v1beta1 = {
  deployment: v1beta1Deployment,
};

export const v1beta2 = {
  deployment: v1beta2Deployment,
};
