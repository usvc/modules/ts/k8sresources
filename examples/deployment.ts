import {createDeployment} from '../src';

const deployment = createDeployment('apps/v1', {name: 'test'});
deployment.spec.template.spec.containers.push(
  {
    image: 'zephinzer/demo-echoserver:latest',
    imagePullPolicy: 'IfNotPresent',
    name: 'echoserver',
  },
);

export default deployment;
