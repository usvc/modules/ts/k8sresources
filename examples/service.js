const {createService} = require('../lib');

const service = createService({name: 'test'});
service.spec.ports = [];
service.spec.ports.push({
  name: 'http',
  port: 11111,
  targetPort: '11111',
});

module.exports = service;
