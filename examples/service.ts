import {createService} from '../src';

const service = createService({name: 'test'});
service.spec.ports = [{
  port: 11111,
  targetPort: '11111',
}];

export default service;
