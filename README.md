# `k8sresources`

This repository contains constructors for common Kubernetes resources.

# Usage

## Installation for Development

```sh
npm install -D k8sresources @types/node;
```

## For appliying with `kubectl`

To apply the JavaScript-based templates, use the [`k8sjs`](https://www.npmjs.com/package/k8sjs) package.

Install it using:

```sh
npm install -g k8sjs
```

And you can generate the template using:

```sh
kubectl js template ./path/to/yourfile.js
```

To apply it directly:

```sh
kubectl js template ./path/to/yourfile.js | kubectl apply -f -
```

To dump the output into a yaml manifest:

```sh
kubectl js template ./path/to/yourfile.js > ./path/to/yourfile.yaml
```

## Using

### With JavaScript

```js
const {createDeployment} = require('k8sresources');

const deployment = createDeployment('apps/v1', {name: 'test'});
deployment.spec.template.spec.containers.push(
  {
    image: 'zephinzer/demo-echoserver:latest',
    imagePullPolicy: 'IfNotPresent',
    name: 'echoserver',
  },
);

module.exports = deployment;

// if you'd like to see the output:
// console.info(JSON.stringify(deployment, null, 2));
```

### With TypeScript

```typescript
import {createDeployment} from 'k8sresources';

const deployment = createDeployment('apps/v1', {name: 'test'});
deployment.spec.template.spec.containers.push(
  {
    image: 'zephinzer/demo-echoserver:latest',
    imagePullPolicy: 'IfNotPresent',
    name: 'echoserver',
  },
);

export default deployment;

// if you'd like to see the output:
// console.info(JSON.stringify(deployment, null, 2));
```

# Roadmap

## Version 1

This package should be able to generate templates for the following Kubernetes resources:

### Application
- [x] [Deployments (v0.1.3)](./examples/deployment.js)
- [x] [Services (v0.2.6)](./examples/service.js)
- [ ] Ingresses

### Configuration
- [ ] ConfigMaps
- [ ] Secrets

### Storage
- [ ] PersistentVolume
- [ ] PersistentVolumeClaim

### RBAC
- [ ] ServiceAccount
- [ ] Role
- [ ] RoleBinding
- [ ] ClusterRole
- [ ] ClusterRoleBinding

# Development Runbook

## Development of Code

1. Code is located in the `./src` directory
2. Before testing changes, run `npm run build` 
3. Use the code in `./examples` to test out your changes

## Building

1. The build process is a little hacky, final build is stored in `./lib`
2. To trigger the build process, use `npm run build`.

## Publishing

1. Bump the version in the root `package.json`
2. Run the build: `npm run build`
3. Navigate into the `./lib` directory
4. Publish the NPM package: `npm publish`

# License

This code is licensed under [the MIT license](./LICENSE).
